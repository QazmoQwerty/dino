#!/bin/bash
echo "building dino..."

# sudo clang++ -g -O0 -Wno-unknown-warning-option `llvm-config --cxxflags --ldflags --system-libs --libs core` -fcxx-exceptions src/*.cpp src/*/*.cpp src/*/*/*.cpp -o /usr/local/bin/dino

# sudo clang++ -g -O0 -include-pch src/CodeGenerator/LlvmInclude.h.gch -Wno-unknown-warning-option `llvm-config --cxxflags --ldflags --system-libs --libs core` -fcxx-exceptions src/*.cpp src/*/*.cpp src/*/*/*.cpp -o /usr/local/bin/dino

cd build
ninja
cd ..

if [ $? -ne 0 ]
then 
    echo "build failed!"
else 
    echo "build succeeded!"
    if [ "$#" -ne 0 ]
    then
        sudo cp build/dino /usr/local/bin/dino
        echo "testing..."
        cd "Examples"
        sh test.sh
        cd ..
    fi
fi
