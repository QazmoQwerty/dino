#include "ErrorReporter.h"

namespace ErrorReporter 
{
    vector<Diagnostic> errors;

    Diagnostic& report(Diagnostic diag)
    {
        errors.push_back(diag);
        diag.print(std::cerr);
        return errors.back();
    }

    Diagnostic& report(string msg, string subMsg, ErrorCode code, Position pos) 
    {
        // TODO - specific error codes
        if (ERR_GENERAL <= code && code < WRN_GENERAL)
            return report(Error(msg, subMsg, pos));

        if (WRN_GENERAL <= code && code < NOTE_GENERAL)
            return report(Warning(msg, subMsg, pos));

        if (NOTE_GENERAL <= code && code  < HELP_GENERAL)
            return report(Note(msg, subMsg, pos));

        if (HELP_GENERAL <= code && code < ERR_UNKNOWN)
            return report(Help(msg, subMsg, pos));

        return report(InternalError(msg, subMsg, pos));
    }

    Diagnostic& report(string msg, ErrorCode code, Position pos) 
    {
        return report(msg, "", code, pos);
    }

    void reportAbort() 
    {
        report(Note("aborting due to previous error"));
    }

    Diagnostic& reportInternal(string msg, Position pos)
    {
        errors.push_back(InternalError(msg, pos));
        errors.back().print(std::cerr);
        return errors.back();
    }

    void showAll() 
    {
        for (auto &i : errors)
            i.print(std::cerr);
    }
}