
#pragma once

#include <iostream>
#include <string>
#include <exception>
#include <limits>
#include <vector>
#include <fstream>
#include <functional>
#include <algorithm>

#include "../SourceFile/SourceFile.h"
#include "../TerminalColors.h"

using std::string;
using std::exception;
using std::vector;

#define FATAL_ERROR(_string) throw ErrorReporter::reportInternal(std::string(_string) + " at " + __func__ + ":" + std::to_string(__LINE__) + " in " + __FILE__)

#define TODO FATAL_ERROR("TODO reached");
#define UNREACHABLE FATAL_ERROR("unreachable reached");
#define ASSERT(cond) if(!(cond)) FATAL_ERROR("assertion failed")

enum ErrorCode {
    ERR_INTERNAL = 0,
    ERR_GENERAL  = 1000,
    WRN_GENERAL  = 2000,
    WRN_IGNORE_LINEBREAK_HAS_NO_EFFECT,
    NOTE_GENERAL = 3000,
    HELP_GENERAL = 4000,
    ERR_UNKNOWN  = 5000,
};

namespace ErrorReporter 
{
    bool hasHelpArticle(ErrorCode errTy);

    typedef reporter::Diagnostic Diagnostic;
    typedef reporter::Error Error;
    typedef reporter::Warning Warning;
    typedef reporter::Note Note;
    typedef reporter::Help Help;
    typedef reporter::InternalError InternalError;
    typedef reporter::Location Position;

    /*
        Static class which contains a vector of Errors. 
        Any compile errors/warnings found will be reported here.
    */
    extern vector<Diagnostic> errors;

    const Position POS_NONE = {};

    /* Pretty-prints all errors reported so far */
    void showAll();

    /* Pretty-prints an Error. */
    void show(Error &err);

    /* 
        Create and save an error/warning.
        Returns the Error created.
    */
    Diagnostic& report(Diagnostic diag);
    Diagnostic& report(string msg, string subMsg, ErrorCode code, Position pos);
    Diagnostic& report(string msg, ErrorCode code, Position pos);

    void reportAbort();

    /*
        Report an ERR_INTERNAL error - these should never be shown to a user of the compiler (in theory at least).
    */
    Diagnostic& reportInternal(string msg, Position pos = POS_NONE);
}