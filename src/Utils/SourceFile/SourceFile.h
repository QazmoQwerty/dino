#pragma once

#include <string>
#include <iostream>
#include "llvm/IR/DIBuilder.h"
#include "../ErrorReporter/reporter.hpp"

#include <stdio.h>
// #include <conio.h>
#include <stdlib.h>
// #include <direct.h>

using std::string;

class SourceFile : public reporter::SourceFile {
private:

    string _path;
public:
    /* Debug Info File */
    llvm::DIFile *_backendDIFile;
    
    SourceFile(string path);

    string str();
    string getName();
    string getPath();
};