#include "CLI.h"

namespace CLI 
{
    bool isErrorCode(char* str)
    {
        if (std::char_traits<char>::length(str) != 4)
            return false;
        switch (str[0])
        {
            case 'e': case 'E': case 'w': case 'W': case 'n': case 'N':
                return '0' <= str[1] && str[1] <= '9' &&
                       '0' <= str[2] && str[2] <= '9' &&
                       '0' <= str[3] && str[3] <= '9';
            default: return false;
        }
        
    }

    void showErrorHelp(char* str)
    {
        uint num = (str[1] - '0') * 100 + (str[2] - '0') * 10 + (str[3] - '0');
        switch (str[0])
        {
            case 'e': case 'E': num += ERR_GENERAL; break;
            case 'w': case 'W': num += WRN_GENERAL; break;
            case 'n': case 'N': num += NOTE_GENERAL; break;
            default: throw "no help article found for `" + string(str) + "`";
        }
        switch ((ErrorCode)num)
        {
            case WRN_IGNORE_LINEBREAK_HAS_NO_EFFECT:
                std::cout << BOLD("\nHelp for: \n    `")
                          << BOLD(FYEL("Warning(W101): ")) << BOLD("line break negator `..` has no effect`\n\n")
                          << "The operator `..` causes the compiler to ignore any newlines occuring before or after the `..`.\n"
                          << "This is used to split a single statement into multiple lines.\n\n"
                          << BOLD(FBLU("  ╭─ ")) << BOLD("Correct") << BOLD(FBLU(" ─╴")) << "\n"
                          << BOLD(FBLU("  │ ")) << "\n"
                          << BOLD(FBLU("5 │ ")) << "    a.." << BOLD(FBLK(" <-- line break is ignored")) << "\n"
                          << BOLD(FBLU("6 │ ")) << "        := 10\n"
                          << BOLD(FBLU("  │ ")) << "\n"
                          << BOLD(FBLU("──╯ ")) << "\n\n"
                          << "A warning is shown when `..` " << ITLC("doesn't")  << " come before/after a newline, and therefore has no effect:\n\n"
                          << BOLD(FBLU("  ╭─ ")) << BOLD("Incorrect") << BOLD(FBLU(" ─╴")) << "\n"
                          << BOLD(FBLU("  │ ")) << "\n"
                          << BOLD(FBLU("5 │ ")) <<           "    a := ..10" << "\n"
                          << BOLD(FBLU("  │ ")) << BOLD(FBLK("         ^^ has no effect, since it does not come before/after a newline")) << "\n"
                          << BOLD(FBLU("──╯ ")) << "\n\n"
                          << BOLD(FBLK("Note:")) << " `..` might easily be confused with the vararg type operator `...`:\n\n"
                          << BOLD(FBLU("  ╭─ ")) << BOLD("Example") << BOLD(FBLU(" ─╴")) << "\n"
                          << BOLD(FBLU("  │ ")) "\n"
                          << BOLD(FBLU("5 │ ")) <<           "    void Foo(int.. n) {" << "\n"
                          << BOLD(FBLU("  │ ")) << BOLD(FBLK("                ^^ should be `...`")) << "\n"
                          << BOLD(FBLU("──╯ ")) << "\n"; break;
            
            default: throw "no help article found for `" + string(str) + "`";
        }
    }
}